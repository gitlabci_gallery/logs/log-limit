[TOC]

# The problem at hand

Sometimes if the log is too verbosy, we may end up with a warning message (see [here](Images/ScreenshotMessage.png) for an illustration).

````
Job's log exceeded limit of 4194304 bytes.
Job execution will continue but no more output will be collected.
````

You may consider it is not that important... but it means that if your job fails after this message, you can't know what went awry (this happens if you uncomment the line raising an exception in the provided `fake_lengthy_text.py` script).

# First possibility: increase the threshold on your runner.

The most common suggestion you will find by searching this issue on the web is to set up your runner configuration to increase the threshold (see for instance [here](https://datawookie.dev/blog/2021/07/fixing-truncated-logs-on-gitlab-ci-cd/)), but this is not an option if you're using a runner you have no control of (typically a shared runner). And even so your new threshold may not be future-proof if your log keeps getting longer.

# Second possibility: using a file and artifact

The Gitlab-CI yaml file for which I get the issue looks like:

````
test:
  tags:
    - ci.inria.fr
    - linux
    - small
  image: 
    ubuntu:latest
  stage: build
  script:
    - apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q && apt-get -y -q autoclean && apt-get -y -q autoremove
    - apt-get install --no-install-recommends -y python3 python3-pip
    - python3 -m pip install --upgrade pip && python3 -m pip install faker
    - python3 fake_lengthy_text.py
````    

The idea to circumvent the limitation in the browser is:

* To also output the content of the log in a file.
* To provide this resulting file as an artifact.

So the corrected Yaml file would look like:

````
test:
  tags:
    - ci.inria.fr
    - linux
    - small
  image: 
    ubuntu:latest
  stage: build
  artifacts:
        name: "log_artifacts"
        expire_in: 5 days
        when: always
        paths:
            - log.txt
  script:
    - apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q && apt-get -y -q autoclean && apt-get -y -q autoremove
    - apt-get install --no-install-recommends -y python3 python3-pip
    - python3 -m pip install --upgrade pip && python3 -m pip install faker
    - python3 fake_lengthy_text.py 2>1 | tee log.txt
````

## Redirect output of several commands

Of course here I chose to output only the output of the script, you may refine it and choose to output this way many lines. You may even choose a different log file for each line in script if you wish; if you do so don't forget to add the paths in the `artifacts` section.

If you want to put the output of several commands in the same log file, don't forget as well to use the `-a` option for `tee`. For instance if here the script is called twice; it would be:

````
- python3 fake_lengthy_text.py 2>1 | tee log.txt
- python3 fake_lengthy_text.py 2>1 | tee -a log.txt
````

## Syntactic sugar... not recommended

A final note: there is a more sugary syntax to do the same which is:

````
- python3 fake_lengthy_text.py |& tee log.txt
````

However it is not as widely supported (it requires Bash 4 and some docker image are clearly not there yet) and so I advise you to stick with the (slightly) more verbosy syntax above.